const pages = [
    'Aftermath',
    'All together now',
    'Believe me',
    'Brain on fire',
    'five_feet_appart',
    'Flocons d amour',
    'index',
    'Isi_et_ossi',
    'Love hard',
    'Moxie',
    'Notre été',
    'Penguin bloom',
    'Plus rien à f',
    'Si tu savais',
    'Taken',
    'The giver',
    'the_kissing_booth',
    'To the bone',
    'Tous nos jours parfaits',
    'Unicorn store',
    'Vinterviken'

];

const search = document.getElementById('search');
search.addEventListener('keyup', (event) => {
    if(event.key === 'Enter' && pages.find(element => element === event.target.value)) {
        window.location = event.target.value + '.html';
    }
});